// TODO: Figure out if pieces are enums.
// A basic piece has a name and moves, this can be extended or given traits to define a variety of pieces.
struct Piece<'a> {
    name: &'a str,
    moves: Vec<(i32, i32)>,
    // Capturing: Vec<(i32, i32)> // How do we implement pawns in normal chess?
    // Influence: Vec<(i32, i32)> // These are the squares the piece has influence over (checks kings?)
    // Type: &str // This is the units type (if you need to collect types to win)
    // Immortal: bool // A flag for the capture rules to check.
    // Precious: bool // bad naming, I mean a piece like the king in chess which is a win condition.
    // Owner: i32 // In case defining a multiplayer game is no more difficult than a two player game.
}
// The grid is just a grid but each cell contains all the information of the game state.
struct Grid<'a> {
    squares: [[&'a str; 8]; 8], // Should be variable sized but I dno the elegant way atm.
}
// TODO: I'm not sure if I should always make a new trait or put many functions in one trait.
// This is a grid trait, it checks where you can 'move' whatever is at the given coordinate.
// Note that it should use the forcedMove trait to see if you must move a precious piece.
trait hasMoves {
    fn moves(&self, (i32, i32)) -> Vec<(bool, (i32, i32))>; //legal moves
}
// For games where you can place pieces in your turn rather than moving (go for example).
trait hasPlacements {
    fn placements(&self) -> Vec<(i32, i32)>; // legal placements
}
// Checks 'precious' pieces for being in check. Or if a check condition is fulfilled.
trait inCheck {
    fn inCheck(&self) -> bool;
}
// TODO: Decide if we need a special capture trait for games where you can capture without moving the piece to the spot or in some other unorthodox way. Or if pieces capture differently from moving.
// TODO: Do we also need a influence trait for visuals? (There'd be a subroutine in the oneTurn function that lets you click a piece and see all legal moves/captures and colored influence rsum when applicable).

fn main() {
    println!("Does nothing!");
}
