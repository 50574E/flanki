THIS FILE IS TO BE RENAMED Kappa-spec.txt

Kappa is the first chess-like game to be implemented with the flanki engine.

The game is simple, each player gets 16 pieces:

- 5 Alphas
- 5 Betas
- 3 Kappas
- 3 Kings


Setup
=====
Before gameplay starts a screen is put in the middle two rows of the 8x8 board and both players place their men on their 3 homerows.

The rules governing how you are allowed to place your men have yet to be developed properly.

You can't start with level 3 alphas or betas nor level 2 Kappas.

Once bothe players are ready the screen is removed and play starts.

Play
====
You win once you manage to kill one of your opponents kings, there are no draws.

Kings cannot be killed unless more than one is in check. That is, if only one of your kings is in check then it is immortal.

If you put two or more of your opponents kings in check you must announce it and he must move out of check if possible.

The pieces can be stacked on top of each other in the following way:
- Kings can only ride Kappas and no one can ride a King. This means Kings can at most be level 2.
- Kappas can also only reach level 2.
- Alphas and Betas can reach level 3.
- You can only jump a single piece on top of a stack.
- You can only jump the top piece off the stack with his original movement pattern, not sure if you can capture with this move.

Level 2 Kappas can jump on friendly pieces and switch places with them.

The pieces move in the following way:

- Alpha 1: Like a rook in chess but only one square.
- Alpha 2: Like a rook in chess
- Alpha 3: Like a rook in chess but enemy level two Alphas cannot move past where the level 3 can move, see diagram.
```
,,I     
~~3~~+~~
,,I  l  
,,+--2--
,,I  l  
```
- Beta 1: Like a bishop in chess but only one square.
- Beta 2: Like a bishop in chess
- Beta 3: Like a bishop in chess but can also move one space like a alpha 1.
- Kappa 1: Like a knight in chess
- Kappa 2: Any of the 16 cells with a # in the picture below (the K is where the Kappas is).
```
#####
#ooo#
#oKo#
#ooo#
#####
```
- King 1: One square in any direction.
- King 2: Can still only capture one square away but can move two squares.

When a piece or stack of pieces is captured the owner has them 'in hand' and can use his turn to place a piece.

A piece can only be place on one of the 8 squares surrounding a king:

- King 1: You can only place on the white squares if you're white and black squares if you're black
- King 2: No such restriction.